%global srcname numpy

Name:           python3-%{srcname}
Version:        1.10.4
Release:        4%{?dist}
Summary:        A fast multidimensional array facility for Python 3

# Everything is BSD except for class SafeEval in numpy/lib/utils.py which is Python
# MIT - numpy/random/mtrand
# PSFv2 - doc/scipy-sphinx-theme/_theme/scipy/static/js/copybutton.js (not shipped)
License:        BSD and Python
URL:            http://www.numpy.org/
Source0:        http://downloads.sourceforge.net/%{srcname}/%{srcname}-%{version}.tar.gz

BuildRequires:  lapack-devel
BuildRequires:  gcc-gfortran
BuildRequires:  atlas-devel

%description
NumPy is a general-purpose array-processing package designed to
efficiently manipulate large multi-dimensional arrays of arbitrary
records without sacrificing too much speed for small multi-dimensional
arrays.  NumPy is built on the Numeric code base and adds features
introduced by numarray as well as an extended C-API and the ability to
create arrays of arbitrary type.

There are also basic facilities for discrete fourier transform,
basic linear algebra and random number generation. Also included in
this package is a version of f2py that works properly with NumPy.


%package -n python%{python3_pkgversion}-%{srcname}
Summary:        A fast multidimensional array facility for Python %{python3_version}

License:        BSD
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-nose
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}

%description -n python%{python3_pkgversion}-%{srcname}
NumPy is a general-purpose array-processing package designed to
efficiently manipulate large multi-dimensional arrays of arbitrary
records without sacrificing too much speed for small multi-dimensional
arrays.  NumPy is built on the Numeric code base and adds features
introduced by numarray as well as an extended C-API and the ability to
create arrays of arbitrary type.

There are also basic facilities for discrete fourier transform,
basic linear algebra and random number generation.

%package -n python%{python3_pkgversion}-%{srcname}-f2py
Summary:        f2py for %{srcname}
Requires:       python%{python3_pkgversion}-%{srcname} = %{version}-%{release}
Requires:       python%{python3_pkgversion}-devel
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}-f2py}

%description -n python%{python3_pkgversion}-%{srcname}-f2py
This package includes a version of f2py%{python3_version} that works properly with NumPy.

%prep
%setup -q -n %{srcname}-%{version}
# Strip shbang
find -name \*.py -exec sed -i -e '1{/^#!/d}' {} +
# workaround for rhbz#849713
# http://mail.scipy.org/pipermail/numpy-discussion/2012-July/063530.html
rm numpy/distutils/command/__init__.py && touch numpy/distutils/command/__init__.py

# Atlas 3.8.4 in RHEL6
# Atlas 3.10 library names
#cat >> site.cfg <<EOF
#[atlas]
#library_dirs = %{_libdir}/atlas
#atlas_libs = satlas
#EOF


%build
env ATLAS=%{_libdir} BLAS=%{_libdir} \
    LAPACK=%{_libdir} CFLAGS="%{optflags}" \
    %{py3_build}


%install
#%%{__python} setup.py install -O1 --skip-build --root %%{buildroot}
# skip-build currently broken, this works around it for now
env ATLAS=%{_libdir} FFTW=%{_libdir} BLAS=%{_libdir} \
    LAPACK=%{_libdir} CFLAGS="%{optflags}" \
    %{__python3} setup.py install --root %{buildroot}

# Fix .so permissions
find %{buildroot} -name \*.so -exec chmod 755 {} +

install -D -p -m 0644 doc/f2py/f2py.1 %{buildroot}%{_mandir}/man1/f2py%{python3_version}.1


%check
pushd doc &> /dev/null
PYTHONPATH="%{buildroot}%{python3_sitearch}" PATH=%{buildroot}%{_bindir}:$PATH \
  %{__python3} -c "import pkg_resources, numpy ; numpy.test(verbose=2)" \
%ifarch s390 s390x
|| :
%endif
# don't remove this comment
popd &> /dev/null


%files -n python%{python3_pkgversion}-%{srcname}
%license LICENSE.txt
%doc README.txt THANKS.txt DEV_README.txt COMPATIBILITY site.cfg.example
%{python3_sitearch}/%{srcname}/__pycache__
%dir %{python3_sitearch}/%{srcname}
%{python3_sitearch}/%{srcname}/*.py*
%{python3_sitearch}/%{srcname}/core
%{python3_sitearch}/%{srcname}/distutils
%{python3_sitearch}/%{srcname}/doc
%{python3_sitearch}/%{srcname}/fft
%{python3_sitearch}/%{srcname}/lib
%{python3_sitearch}/%{srcname}/linalg
%{python3_sitearch}/%{srcname}/ma
%{python3_sitearch}/%{srcname}/random
%{python3_sitearch}/%{srcname}/testing
%{python3_sitearch}/%{srcname}/tests
%{python3_sitearch}/%{srcname}/compat
%{python3_sitearch}/%{srcname}/matrixlib
%{python3_sitearch}/%{srcname}/polynomial
%{python3_sitearch}/%{srcname}-*.egg-info

%files -n python%{python3_pkgversion}-%{srcname}-f2py
%{_bindir}/f2py%{python3_version}
%{_mandir}/man1/f2py%{python3_version}.1*
%{python3_sitearch}/%{srcname}/f2py


%changelog
* Mon Oct 17 2016 Tim Orling <ticotimo@gmail.com> - 1.10.4-5
- Atlas is 3.8.4 in RHEL6

* Tue Mar 1 2016 Orion Poplawski <orion@cora.nwra.com> - 1.10.4-4
- Strip shbangs and fix .so permissions

* Fri Feb 26 2016 Orion Poplawski <orion@cora.nwra.com> - 1.10.4-3
- Drop epoch
- Drop BR Cython

* Thu Feb 25 2016 Orion Poplawski <orion@cora.nwra.com> - 1.10.4-2
- Add missing BuildRequires

* Mon Jan 11 2016 Orion Poplawski <orion@cora.nwra.com> - 1.10.4-1
- Initial EPEL7 package
